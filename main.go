package parameters

import (
	"fmt"
	"os"
	"strconv"
)

var (
	Debug                bool
	DurationProxyMinutes float64
	err                  error
	IgnoreGoodProxy      bool
	MaxNumberOfAttempts  int
	ScrapTimeout         int
	UseTor               bool
	WorkerCount          int
	Errors               struct {
		BadGateway             int
		BadRequest             int
		BanByAli               int
		ConnectionProxyconnect int
		ConnectionRefused      int
		ConnectionResetByPeer  int
		EOF                    int
		Forbidden              int
		HTTP2HTTPS             int
		InternalServerError    int
		InvalidHeader          int
		Malformed              int
		NetworkIsUnreachable   int
		Timeout                int
		TLS                    int
		TooManyConnection      int
		Other                  int
	}
)

func init() {
	Debug, err = strconv.ParseBool(os.Getenv("DEBUG"))
	if err != nil {
		Debug = false
	}
	MaxNumberOfAttempts, err = strconv.Atoi(os.Getenv("MAX_NUMBER_OF_ATTEMPTS"))
	if err != nil {
		fmt.Println("MAX_NUMBER_OF_ATTEMPTS is not defined")
		MaxNumberOfAttempts = 10
	}
	ScrapTimeout, err = strconv.Atoi(os.Getenv("TIMEOUT_SCRAP"))
	if err != nil {
		fmt.Println("TIMEOUT_SCRAP is not defined")
		ScrapTimeout = 10
	}
	WorkerCount, err = strconv.Atoi(os.Getenv("WORKER_COUNT"))
	if err != nil {
		fmt.Println("WORKER_COUNT is not defined")
		WorkerCount = 15
	}
	DurationProxyMinutes, err = strconv.ParseFloat(os.Getenv("DURATION_PROXY_MINUTES"), 64)
	if err != nil || DurationProxyMinutes == 0 {
		DurationProxyMinutes = 10
	}
	IgnoreGoodProxy, err = strconv.ParseBool(os.Getenv("IGNORE_GOOD_PROXY"))
	if err != nil {
		IgnoreGoodProxy = false
	}
	UseTor, err = strconv.ParseBool(os.Getenv("USE_TOR"))
	if err != nil {
		UseTor = false
	}
}
